// Aggregiation Pipelines



db.course_bookings.insertMany([

        {"courseId": "C001", "studentId": "S004", "isCompleted": true},
        {"courseId": "C002", "studentId": "S001", "isCompleted": false},
        {"courseId": "C001", "studentId": "S003", "isCompleted": true},
        {"courseId": "C003", "studentId": "S002", "isCompleted": false},
        {"courseId": "C001", "studentId": "S002", "isCompleted": true},
        {"courseId": "C004", "studentId": "S003", "isCompleted": false},
        {"courseId": "C002", "studentId": "S004", "isCompleted": true},
        {"courseId": "C003", "studentId": "S007", "isCompleted": false},
        {"courseId": "C001", "studentId": "S005", "isCompleted": true},
        {"courseId": "C004", "studentId": "S008", "isCompleted": false},
        {"courseId": "C001", "studentId": "S013", "isCompleted": true}
]);


// Aggregiation in MongoDB
	// this is the act or process of generating manipulated data and perform operations to create filtered results that helps in analyzing data.
	// this helps in creating from analysing the data provided in our documents.

// Syntax

	db.collections.aggregates([
			{Stage 1}
			{Stage 2}
			{Stage 3}

	])



// Aggregiation Pipelines

	// Aggregiation is done 2-3 steps typically. The first pipeline was with the use of $match.


	// $match is used to pass the documents	or get the documents that will match our conditions.

		// syntax : {$match:{field: value}}

	// $group is used to group elements/documents together and create an analysis of these grouped documents.
	
		// Syntax: {$group: {_id: <id>, fieldResult: "valueResult"}}

	// $sort
	// $project - this allow us to show or hide details.

		syntax: {$project: {field: 0 or 1}};


// count all the documents in our collection.

	db.course_bookings.aggregate([

			{$group: {_id: null, count: {$sum: 1}}}

	])

	// see all the documents


	db.course_bookings.aggregate([

			{$group: {_id: "$courseId", count: {$sum: 1}}}

	])

	// set to show the count for each unique courseId

		db.course_bookings.aggregate([

			{$match: {"isCompleted": true}},
			{$group: {_id: "$courseId", total: {$sum: 1}}}
		])


		db.course_bookings.aggregate([

			{$match: {"isCompleted": true}},
			{$project: {"courseId" : 0}}
		])



// -1 descending , 1 ascending
		db.course_bookings.aggregate([

			{$match: {"isCompleted": true}},
			{$sort: {"courseId" : -1}}
		])



// Mini ACtivity


db.course_bookings.aggregate([

			{$match: {"studentId": "S013", "isCompleted" : true }},
			{$group: {_id: null, count: {$sum: 1}}}
		])
	
db.orders.insertMany([
        {
                "cust_Id": "A123",
                "amount": 500,
                "status": "A"
        },
        {
                "cust_Id": "A123",
                "amount": 250,
                "status": "A"
        },
        {
                "cust_Id": "B212",
                "amount": 200,
                "status": "A"
        },
        {
                "cust_Id": "B212",
                "amount": 200,
                "status": "D"
        }
])

db.orders.aggregate([

	{$match: {status: "A"}},
	{$group: {_id: "$cust_Id", total: {$sum: "$amount"}}}

	])


// $sum - will total the values
// $avg - will average the results
// $min - will show you the lowest value
// $max - will show you the highest value

db.orders.aggregate([

	{$match: {status: "A"}},
	{$group: {_id: "$cust_Id", averageAmount: {$avg: "$amount"}}}

	])


